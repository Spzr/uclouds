import React, { Component } from 'react';
import $ from "jquery";
import { Container, Row} from 'react-bootstrap';
import TopNav from '../../Components/TopNav/TopNav';
import ScrollDown from '../../Components/ScrollDown/ScrollDown';
import ServiceItem from '../../Components/ServiceItem/ServiceItem'
import SectionTitle from '../../Components/SectionTitle/SectionTitle'
import Params from '../../Components/Params/Params'
import icon1 from '../../Components/ServiceItem/icon1.svg';
import PortfolioBlock from '../../Components/PortfolioBlock/PortfolioBlock'
import './home.scss';

export default class Home extends Component {
  render() {
    return (
      <>
       
       <TopNav />
        <div className="section-1">
        <Container fluid>
          <Row>
            <div className="demo" id="demo">
              <div className="content">
                  <div id="large-header" className="large-header">
                    <canvas id="demo-canvas"></canvas>
                    <div className=" main-title">
                      <h1 className="header-title">UNDERCLOUDS</h1>
                      <p className="header-subtitle">Ex aliquip elit occaecat ut anim aute dolore occaecat amet cupidatat.</p>
                    </div>
                    
                  </div>
              </div>
              <ScrollDown />
              <Params />
            </div>
          </Row>
        </Container>
        
        </div>
        <div className="section-2">
          <Container fluid>
            <SectionTitle pretitle="Что мы делаем?" title="Эффектные &nbsp;и&nbsp; эффективные веб &nbsp;и&nbsp; мобайл решения с погружением в ваш бизнес." />
              <div className="cards-box">
                <ServiceItem 
                cardimage={icon1}
                cardtitle="eCommerce v2.0"
                cardtext="Опытная команда погружается в задачу и создает индивидуальный продукт, согласно целям вашего бизнеса, гибкими agile итерациями. Такое взаимодействие подходит для создания нестандартных решений: маркетплейсов, интернет магазинов, CRM, ERP систем."
                LinkAdress="/services"
                LinkText="Страница услуг"  />
                <ServiceItem 
                cardimage={icon1}
                cardtitle="eCommerce v2.0"
                cardtext="Опытная команда погружается в задачу и создает индивидуальный продукт, согласно целям вашего бизнеса, гибкими agile итерациями. Такое взаимодействие подходит для создания нестандартных решений: маркетплейсов, интернет магазинов, CRM, ERP систем."
                LinkAdress="/services"
                LinkText="Тестовая ссылка"  />
                <ServiceItem
                cardimage={icon1} 
                cardtitle="eCommerce v2.0"
                cardtext="Опытная команда погружается в задачу и создает индивидуальный продукт, согласно целям вашего бизнеса, гибкими agile итерациями. Такое взаимодействие подходит для создания нестандартных решений: маркетплейсов, интернет магазинов, CRM, ERP систем."
                LinkAdress="/services"
                LinkText="Перейти"  />
              </div>
          </Container>
        </div>
        <PortfolioBlock />
    
        
      </>
    )
  }
}
