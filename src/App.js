import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { ParallaxProvider } from 'react-scroll-parallax';

import Home from './Pages/Home/Home';
import Services from './Pages/Services/Services.js';
import Portfolio from './Pages/Portfolio/Portfolio.js';
import News from './Pages/News/News.js';
// import Jquery from 'jquery'



function App() {
  return (
    <ParallaxProvider>
      <Router>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/services" component={Services} />
                <Route exact path="/portfolio" component={Portfolio} />
                <Route exact path="/news" component={News} />
            </Switch>
        </Router>
   </ParallaxProvider>
  );
}

export default App;
