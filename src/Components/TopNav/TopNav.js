import React, { Component } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import logo from './logo.svg';
import PortfolioLogo from './menu.svg'

// import NavbarToggle from 'react-bootstrap/NavbarToggle'
import Portfolio from '../../Pages/Portfolio/Portfolio';


export default class TopNav extends Component {
  render() {
    return (
      <>
        <Navbar collapseOnSelect expand="" bg="transparent" variant="dark" fixed="top" >
            <Container fluid>
                <a href="/" className="navbrand-box">
                    <img
                      src={logo}
                      width="64"
                      className="d-inline-block align top"
                      alt="logo"
                    />
                    <a href="/portfolio"><img
                      src={PortfolioLogo}
                      width="34"
                      className="d-inline-block align top"
                      alt="logo"
                    /></a>
                    
                </a>
                <Navbar.Toggle className="ml-auto" aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link href="/"> Главная</Nav.Link>
                        <Nav.Link href="/services"> Услуги</Nav.Link>
                        <Nav.Link href="/portfolio"> Портфолио</Nav.Link>
                        <Nav.Link href="/news"> Новости </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>

        

        
      </>
    )
  }
}
