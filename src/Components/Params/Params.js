import React, { Component } from 'react'
import './Params.css'

export default class Params extends Component {
    render() {
        return (
            <div className="params-box">
                <div className="params-item delay-0">
                    <div className="num">10</div>
                    <div className="text">Специалистов в команде</div>
                </div>
                <div className="params-item delay-05">
                    <div className="num">4+</div>
                    <div className="text">Лет успешной работы</div>
                </div>
                <div className="params-item delay-1">
                    <div className="num">150+</div>
                    <div className="text">Выполненных проектов</div>
                </div>
            </div>
        )
    }
}
