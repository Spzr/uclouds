import React, { Component } from 'react'

export default class SectionTitle extends Component {
  render() {
    return (
      <div className="title-block">
        <p className="section-pretitle">{this.props.pretitle}</p>
        <h2 className="section-title">{this.props.title}</h2>
      </div>
    )
  }
}
