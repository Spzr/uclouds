import React, { Component } from 'react';
import icon1 from './icon1.svg';

export default class ServiceItem extends Component {
  render() {
    return (
        <div className="card-body">
            <div className="card-image">
                <img src={this.props.cardimage} alt="icon" width="128" height="128"></img>
            </div>
            <div className="card-title">{this.props.cardtitle}</div>
            <div className="card-text">{this.props.cardtext}</div>
            <a className="LinkButton" href={this.props.LinkAdress}>{this.props.LinkText}</a>
        </div>
    )
  }
}
