import React, { Component } from 'react'
import './Strips.css'

export default class Strips extends Component {
    render() {
        return (
            <div className="strips">
               <div className="strip null"></div>
               <div className="strip one"></div>
               <div className="strip two"></div>
               <div className="strip three"></div>
            </div>
        )
    }
}
