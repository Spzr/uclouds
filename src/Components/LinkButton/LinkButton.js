import React, { Component } from 'react'

export default class LinkButton extends Component {
  render() {
    return (
      <>
        <a  className="LinkButton" href={this.props.LinkAdress}>{this.props.LinkText}</a>
      </>
    )
  }
}
