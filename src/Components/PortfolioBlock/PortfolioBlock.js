import React, { Component } from 'react'
import backgroundImage from './book-5280551_1920.jpg';
import Strips from '../../Components/Strips/Strips'
import './custom'
import './custom.css'

export default class PortfolioBlock extends Component {
    render() {
        return (
            <div>
                <div className="section-3">
                <Strips />
                    <div className="container-fluid padding-l">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="content-hero text-left" data-aos="fade-left" >
                                    <h2 className="h-title">
                                        <span className="row">
                                        <span className="stroke-h">WORKS</span>
                                        </span>
                                        <span className="row">
                                        <span data-emergence="visible" className="cursor-item">WORKS</span>
                                        </span>
                                        <span className="row">
                                        <span className="stroke-h">WORKS</span>
                                        </span>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="navigation-wrapper">
                        <div className="project-preview-wrapper">
                            <div className="project-preview">
                                
                            </div>
                        </div>
                        <ul className="navigation-list">
                            <li className="navigation-item">
                                <a className="navigation-link navigation-link-1" href="/">
                                    <span data-text="Piano">Piano</span>
                                </a>
                                <p>Web design</p>
                            </li>
                            <li className="navigation-item">
                                <a className="navigation-link navigation-link-2" href="/">
                                    <span data-text="Piano">Pwwwiano</span>
                                </a>
                                <p>Web dewwwsign</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
