
import $ from 'jquery'
import gsap from 'gsap';
import { TweenMax } from 'gsap';
import { TimelineLite } from 'gsap'
import { Expo } from 'gsap'
import {CSSPlugin} from 'gsap/CSSPlugin'




(function ($) {



    $(document).ready(function () {
		TweenMax.set({className:"project-preview"}, {
			width: 0
        });
        
		var tl = new TimelineLite();

		$(document)
			.on("mouseover", ".navigation-item", function (evt) {
				tl = new TimelineLite();
				tl.to($(".project-preview"), 1, {
					width: "600px",
					ease: Expo.easeInOut
				});
			})
			.on("mouseout", ".navigation-item", function (evt) {
				tl = new TimelineLite();
				tl.to($(".project-preview"), 0.5, {
					width: 0,
					ease: Expo.easeInOut
				});
			});
	});

	$(".navigation-link-1").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(https://via.placeholder.com/500x500)"
		});
	});

	$(".navigation-link-2").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(https://via.placeholder.com/600x600)"
		});
	});

	$(".navigation-link-3").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(img/portfolio/img-3.jpg)"
		});
	});

	$(".navigation-link-4").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(img/portfolio/img-4.jpg)"
		});
	});

	$(".navigation-link-5").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(img/portfolio/img-5.gif)"
		});
	});

	$(".navigation-link-6").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(img/portfolio/img-6.jpg)"
		});
	});

	$(".navigation-link-7").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(img/portfolio/img-7.gif)"
		});
	});

	$(".navigation-link-8").on("mouseover", function () {
		$(".project-preview").css({
			"background-image": "url(img/portfolio/img-8.jpg"
		});
	});


	// go to top
	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 100) {
			$('#back-to-top').fadeIn();
		} else {
			$('#back-to-top').fadeOut();
		}
	});
	// scroll body to 0px on click
	$('#back-to-top').on('click', function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});


})($);